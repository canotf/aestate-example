<h1 align="center">
使用 aestate 制作一个论坛
</h1>

# 使用的技术

前端：

- vue2
- axios
- echarts
- element-ui
- vuex
- vue-router

后端

- aestate==1.0.1
- aestate-json==1.0.0
- APScheduler==3.7.0
- blueprint==3.4.2
- Flask-APScheduler==1.12.2
- Flask-Cors==3.0.10
- PyMySQL==1.0.2
