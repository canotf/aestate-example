from db_base.db_impl import MyFactory

ArticleOrm = MyFactory.createInstance('article.Article')
ClassificationOrm = MyFactory.createInstance('article.Classification')
StarOrm = MyFactory.createInstance('article.Star')
CommentOrm = MyFactory.createInstance('article.Comment')
UserOrm = MyFactory.createInstance('user.User')
