/*
 Navicat Premium Data Transfer

 Source Server         : aliyun
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : cacode-database.mysql.rds.aliyuncs.com:3306
 Source Schema         : luntan

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 07/06/2021 14:27:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classification_id` int(11) NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `classification_id`(`classification_id`) USING BTREE,
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `article_ibfk_2` FOREIGN KEY (`classification_id`) REFERENCES `classification` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES (45, 3, '2021-06-07 06:25:55.000000', '2021-06-07 06:25:55.000000', 3, '<p>的撒大大</p>', '33333');

-- ----------------------------
-- Table structure for article_comment
-- ----------------------------
DROP TABLE IF EXISTS `article_comment`;
CREATE TABLE `article_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `article_id`(`article_id`) USING BTREE,
  CONSTRAINT `article_comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `article_comment_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_comment
-- ----------------------------

-- ----------------------------
-- Table structure for article_star
-- ----------------------------
DROP TABLE IF EXISTS `article_star`;
CREATE TABLE `article_star`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `article_id`(`article_id`) USING BTREE,
  CONSTRAINT `article_star_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `article_star_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_star
-- ----------------------------

-- ----------------------------
-- Table structure for classification
-- ----------------------------
DROP TABLE IF EXISTS `classification`;
CREATE TABLE `classification`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classification
-- ----------------------------
INSERT INTO `classification` VALUES (3, '2021-06-07 11:40:41', '2021-06-07 11:40:41', '测试增加分类');
INSERT INTO `classification` VALUES (4, '2021-06-07 11:42:09', '2021-06-07 11:42:09', '测试修改分类');
INSERT INTO `classification` VALUES (5, '2021-06-07 04:01:23', '2021-06-07 04:01:23', '测试3333');
INSERT INTO `classification` VALUES (6, '2021-06-07 04:04:32', '2021-06-07 04:04:32', '测试一个');
INSERT INTO `classification` VALUES (7, '2021-06-07 04:17:35', '2021-06-07 04:17:35', '414141');
INSERT INTO `classification` VALUES (8, '2021-06-07 04:49:41', '2021-06-07 04:49:41', '33333');
INSERT INTO `classification` VALUES (9, '2021-06-07 04:49:43', '2021-06-07 04:49:43', '33333');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` int(11) NOT NULL DEFAULT 0,
  `u_type` tinyint(255) NOT NULL DEFAULT 0,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`, `email`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '由比滨结衣', '123456', 'cacode@163.com', 1, 0, '2021-06-03 22:55:15.000000', '2021-06-03 22:55:15.000000', 'default.png');
INSERT INTO `user` VALUES (2, '初春饰利', '123456', '2075383131@qq.com', 1, 0, '2021-06-03 23:03:14.000000', '2021-06-03 23:03:14.000000', 'default.png');
INSERT INTO `user` VALUES (3, '秋庭里香4', '123456', 'admin@cacode.ren', 0, 1, '2021-06-05 14:37:53.000000', '2021-06-05 14:37:53.000000', 'default.png');
INSERT INTO `user` VALUES (4, '汤浅比吕美', 'asdasdasd', 'aaa@aa.com', 0, 0, '2021-06-06 06:17:31.000000', '2021-06-06 06:17:31.000000', 'default.png');
INSERT INTO `user` VALUES (5, '三日月夜空', '1234567', 'aaaaa@a.com', 2, 0, '2021-06-06 06:18:42.000000', '2021-06-06 06:18:42.000000', 'default.png');
INSERT INTO `user` VALUES (6, '秋庭里香', '123456789', 'af@a.com', 1, 0, '2021-06-06 06:20:03.000000', '2021-06-06 06:20:03.000000', 'default.png');
INSERT INTO `user` VALUES (7, '藤和艾莉欧', '123456789', 'qq@qq.com', 2, 0, '2021-06-06 06:20:46.000000', '2021-06-06 06:20:46.000000', 'default.png');
INSERT INTO `user` VALUES (8, '木山春生', '123456789', 'gggg@qq.qq', 2, 0, '2021-06-05 22:31:04.000000', '2021-06-05 22:31:04.000000', 'default.png');
INSERT INTO `user` VALUES (9, '婚后光子', '123456789', 'qqqq@www.cm', 2, 0, '2021-06-06 13:41:52.000000', '2021-06-06 13:41:52.000000', 'default.png');
INSERT INTO `user` VALUES (10, '汤浅比吕美', '1234567', '1255@qq.com', 2, 0, '2021-06-06 13:53:41.000000', '2021-06-06 13:53:41.000000', 'default.png');
INSERT INTO `user` VALUES (11, '哈哈哈皮', '15879093053', '1348977728@qq.com', 2, 0, '2021-06-06 13:58:35.000000', '2021-06-06 13:58:35.000000', 'default.png');

SET FOREIGN_KEY_CHECKS = 1;
