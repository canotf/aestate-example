from aestate.work.annos import Table
from aestate.work import Manage

from db_base.db_impl import DatabaseTemplate


@Table(name='article', msg='文章表')
class Article(DatabaseTemplate):
    """
    帖子
    """

    def __init__(self, **kwargs):
        self.title = Manage.tag.varcharField(length=255, comment='标题')
        self.content = Manage.tag.longtextField(comment='帖子的内容')
        self.user_id = Manage.tag.varcharField(length=50, comment="用户的id")
        self.classification_id = Manage.tag.intField(comment='帖子的分类')
        super(Article, self).__init__(**kwargs)


@Table(name='classification', msg='分类表')
class Classification(DatabaseTemplate):
    """
    分类名称
    """

    def __init__(self, **kwargs):
        self.name = Manage.tag.varcharField(length=50, comment='帖子的id')
        super(Classification, self).__init__(**kwargs)


@Table(name='article_star', msg='点赞表')
class Star(DatabaseTemplate):
    """
    赞
    """

    def __init__(self, **kwargs):
        self.article_id = Manage.tag.intField(comment='帖子的id')
        self.user_id = Manage.tag.intField(comment='用户id')
        super(Star, self).__init__(**kwargs)


@Table(name='article_comment', msg='评论表')
class Comment(DatabaseTemplate):
    """
    评论
    """

    def __init__(self, **kwargs):
        self.article_id = Manage.tag.intField(comment='帖子的id')
        self.user_id = Manage.tag.intField(comment='用户id')
        self.content = Manage.tag.longtextField(comment='评论内容')
        super(Comment, self).__init__(**kwargs)
