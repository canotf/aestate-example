import random

from aestate.work.annos import Table
from aestate.work import Manage

from db_base.db_impl import DatabaseTemplate

names = [i.replace(' ', '').replace('\n', '') for i in """
    司波深雪|雪之下雪乃|由比滨结衣|杏本诗歌|秋庭里香|三日月夜空|樱野栗梦|藤和艾莉欧|紫苑寺有子|姬路瑞希|千鸟要|七海春歌|小鸟游六花|椎名真白|朝比奈凉风|石动乃绘|汤浅比吕美|立华奏|
    向井户爱花|比良平千咲|友利奈绪|见崎鸣|呱太|御坂美琴|白井黑子|初春饰利|佐天泪子|上条当麻|婚后光子|湾内绢保|泡浮万彬|固法美伟|木山春生
    """.split('|')]


@Table(name='user', msg='用户表')
class User(DatabaseTemplate):
    def __init__(self, **kwargs):
        SEX = (
            (0, '男'),
            (1, '女'),
            (2, '中性'),
            (3, '无性'),
        )
        # 名称随机从里面取一个
        self.avatar = Manage.tag.varcharField(length=255, default='default.png', comment='头像')
        self.name = Manage.tag.varcharField(length=50,
                                            default=names[random.randint(0, len(names) - 1)],
                                            comment='用户昵称')
        self.password = Manage.tag.varcharField(length=50, comment='密码')
        self.email = Manage.tag.varcharField(is_null=True, comment='邮箱')
        self.u_type = Manage.tag.tinyintField(is_null=True, comment='0普通用户1管理员')
        self.sex = Manage.tag.intField(default=SEX[random.randint(0, len(SEX) - 1)][0], comment='性别')

        super(User, self).__init__(**kwargs)
