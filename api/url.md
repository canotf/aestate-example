用户性别：   
SEX = (
(0, '男'),
(1, '女'),
(2, '中性'),
(3, '无性'),
)

# 帖子

帖子路由：

- 帖子类别：/article/types/ GET

```json
[
  {
    "name": "分类的名称",
    "id": 1,
    "create_time": "2021-06-02 23:29:35",
    "update_time": "2021-06-02 23:29:35"
  },
  {
    "name": "分类的名称",
    "id": 2,
    "create_time": "2021-06-02 23:32:34",
    "update_time": "2021-06-02 23:32:38"
  }
]
```

- 帖子列表：/article/list/<类别>/ GET

```json
{
  "code": 200,
  "message": "ok",
  "data": [
    {
      "title": "我是标题",
      "content": "我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容我是内容",
      "user_id": 1,
      "classification_id": 1,
      "id": 11,
      "create_time": "2021-06-03 18:34:00",
      "update_time": "2021-05-02 18:48:54",
      "star_count": 1,
      "comment_count": 4
    }
  ],
  "listQuery": {
    "page": 1,
    "limit": 10
  },
  "total": 24
}
```

获取指定帖子：/article/item/<帖子的id>/ GET

```json
{
  "title": "\u6211\u662f\u6807\u9898",
  "content": "\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9\u6211\u662f\u5185\u5bb9",
  "user_id": 1,
  "classification_id": 1,
  "id": 1,
  "create_time": "2021-06-03 18:34:00",
  "update_time": "2021-05-02 18:34:00"
}
```

- 发表评论：/article/speak/ POST  
  请求参数：

```json
{
  "user_id": "用户id type:int",
  "content": "评论内容 type:string",
  "article_id": "评论的帖子id type:int"
}
```

返回结果：

```json
{
  "article_id": 1,
  "user_id": "1",
  "content": "评论内容",
  "id": 4,
  "create_time": "2021-06-03 00:44:44",
  "update_time": "2021-06-03 00:44:44"
}
```

获取帖子的评论：/article/comment/ GET   
请求参数

```json
{
  "article_id": 1,
  "page": 1,
  "limit": 10
}
```

返回结果

```json
{
  "listQuery": {
    "page": 1,
    "limit": 10
  },
  "total": 4,
  "data": [
    {
      "article_id": 1,
      "user_id": "1",
      "content": "<p>测试提交评论</p>",
      "id": 5,
      "create_time": "2021-06-05 07:50:29",
      "update_time": "2021-06-05 07:50:29",
      "user": {
        "avatar": "default.png",
        "name": "由比滨结衣",
        "password": "123456",
        "email": "cacode@163.com",
        "u_type": 0,
        "sex": 1,
        "id": 1,
        "create_time": "2021-06-03 22:55:15",
        "update_time": "2021-06-03 22:55:15"
      }
    }
  ]
}
```

点赞：/article/star/ POST

```json
{
  "article_id": "帖子的id type:int",
  "user_id": "用户的id type:int"
}
```

发帖： /article/create/ POST  
请求参数

```json

{
  "title": "标题",
  "content": "内容",
  "user_id": 1,
  "classification_id": "分类的id"
}
```

返回

```json
{
  "title": "标题",
  "content": "内容",
  "user_id": 1,
  "classification_id": 1,
  "id": 22,
  "create_time": "2021-06-05 12:31:47",
  "update_time": "2021-06-05 12:31:47"
}
```

获得指定用户的发帖：/user_article/ GET

```json
[
  {
    "content": "帖子的内容",
    "user_id": 1,
    "classification_id": 1,
    "id": 1,
    "create_time": "2021-05-02 18:34:00",
    "update_time": "2021-05-02 18:34:00"
  }
]
```

用户最近七天发帖情况的echarts：/article/user_visual/ GET  
请求参数

```json
{
  "user_id": 1
}
```

返回option:

```json
{
  "option": {
    "title": {
      "show": true,
      "text": "\u6700\u8fd1\u4e03\u5929\u53d1\u5e16\u9891\u7387"
    },
    "xAxis": {
      "type": "category",
      "data": [
        "2021-06-03",
        "2021-06-02",
        "2021-06-01",
        "2021-05-31",
        "2021-05-30",
        "2021-05-29",
        "2021-05-28"
      ]
    },
    "yAxis": {
      "type": "value"
    },
    "series": [
      {
        "data": [
          17,
          1,
          1,
          1,
          0,
          0,
          0
        ],
        "type": "line"
      }
    ]
  }
}
```

# 用户

登录 /user/login/ POST  
请求参数

```json
{
  "email": "cacode@163.com",
  "password": "123456",
  "pwd": "123456"
}
```

返回

```json
{
  "avatar": "/default.png",
  "name": "\u5471\u592a",
  "password": "123456",
  "email": "cacode@163.com",
  "sex": 0,
  "id": 1,
  "create_time": "2021-05-05 16:03:57",
  "update_time": "2021-05-05 16:03:57"
}
```

注册：/user/join/ POST

请求参数

```json
{
  "email": "cacode@163.com",
  "password": "123456",
  "pwd": "123456"
}
```

返回:

```json
{
  "message": "邮箱已被注册",
  "code": 400
}
```

```json
{
  "message": "两次输入密码不一致",
  "code": 400
}
```

```json
{
  "avatar": "default.png",
  "name": "\u521d\u6625\u9970\u5229",
  "password": "123456",
  "email": "2075383131@qq.com",
  "sex": 2,
  "id": 2,
  "create_time": "2021-06-03 23:03:14",
  "update_time": "2021-06-03 23:03:14"
}
```

上传头像： /avatar/ POST   
请求参数：   
头像文件的key：avatar  
用户id：user_id

访问头像： /avatar/<文件名>/ GET

---

修改个人信息：/user/update/ POST

改了什么就更新什么，必填字段：

- email
- password

```json
{
  "avatar": "default.png",
  "name": "由比滨结衣",
  "password": "123456",
  "email": "cacode@163.com",
  "u_type": 0,
  "sex": 1,
  "id": 1,
  "create_time": "2021-06-03 22:55:15",
  "update_time": "2021-06-03 22:55:15"
}
```

# 管理员

最近七天各个分类的发帖数量：/admin/visual/sum/ GET

```text
返回ecahrt的option参数
```

各个分类的发帖数量：/admin/visual/cfsum/ GET

```text
返回ecahrt的option参数
```

各个分类的评论数量：/visual/comment/ GET

```text
返回ecahrt的option参数

各个板块占比：/visual/pie/ GET

```text
返回ecahrt的option参数
```

修改帖子：/admin/updateArticle/ PUT

请求参数

```json
{
  "title": "我是标题",
  "content": "测试修改内容",
  "user_id": 1,
  "classification_id": 1,
  "id": 2,
  "create_time": "2021-06-02 18:34:00",
  "update_time": "2021-05-02 18:35:31"
}
```

返回结果：

```json

```

删除帖子：/admin/updateArticle/ DELETE

请求参数：

```json
{
  "article_id": 1
}
```

返回结果：

```json
[
  0,
  0,
  0,
  0,
  0,
  0
]
```

# 分类

删除分类：/admin/updateCf/ DELETE

请求参数

```json
{
  "id": 1
}
```

返回：

```json
[
  4,
  0,
  7,
  0,
  1,
  0
]
```

增加：/admin/updateCf/ POST

请求参数：

```json
{
  "name": "测试增加分类"
}
```

返回

```json
  {
  "name": "测试增加分类",
  "id": 3,
  "create_time": "2021-06-07 11:40:41",
  "update_time": "2021-06-07 11:40:41"
}
```

修改分类：/admin/updateCf/ PUT

请求参数：

```json
{
  "name": "测试增加分类",
  "id": 3,
  "create_time": "2021-06-07 11:40:41",
  "update_time": "2021-06-07 11:40:41"
}
```

返回参数

```json
{
  "data": {
    "name": "测试修改分类",
    "id": 4,
    "create_time": "2021-06-07 11:42:09",
    "update_time": "2021-06-07 11:42:09"
  },
  "code": 200,
  "message": "ok"
}
```
