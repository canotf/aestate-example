# -*- utf-8 -*-
# @Time: 2021/6/2 23:36
# @Author: CACode
from ajson import aj
from flask import Response


def Error(msg, code=500):
    result = {
        'message': msg,
        'code': code
    }
    return Response(aj.parse(result), status=code)
