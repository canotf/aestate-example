# -*- utf-8 -*-
# @Time: 2021/6/4 9:31
# @Author: CACode
import random

from views import color


class BarEcharts:
    def __init__(self, xData: list, yData: list, title: str):
        self.option = {
            'color': color.one(),
            'tooltip': {
                'axisPointer': {
                    'type': 'shadow'
                },
                'trigger': 'item',
            },
            "title": {
                "show": True,
                "text": title
            },
            'xAxis': {
                'type': 'category',
                'data': xData
            },
            'yAxis': {
                'type': 'value'
            },
            'series': [{
                'data': yData,
                'type': 'bar',
            }]
        }


class PieEcharts:
    def __init__(self, data: list, title: str):
        self.option = {
            'title': {
                'text': title,
                'left': 'center'
            },
            'tooltip': {
                'trigger': 'item',
                'formatter': "{a} <br/>{b}: {c} ({d}%)"
            },
            'legend': {
                'orient': 'vertical',
                'left': 'left',
            },
            'color': color.many(len(data)),
            'series': [
                {
                    'name': title,
                    'type': 'pie',
                    'center': ['50%', '50%'],
                    'itemStyle': {
                        'borderRadius': 8
                    },
                    'label': {
                        'normal': {
                            'show': True,
                            'position': 'inside',
                            'formatter': '{d}%',

                            'textStyle': {
                                'align': 'center',
                                'baseline': 'middle',
                                'fontWeight': 'bolder'
                            }
                        },
                    },
                    'data': data
                }
            ]
        }
