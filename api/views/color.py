import random

BASE_COLORS = [
    '#4962FC', '#4B7CF3', '#dd3ee5', '#12e78c', '#fe8104', '#01C2F9', '#F4CB29', '#FD9E06',
    '#d223e7', '#f376e0', '#4962FC', '#768BFF', '#3F77FE', '#02CBF9', '#4962FC', '#189CBF',
    '#18D070', '#12ED93', '#01C2F9', '#3FD0F9',
]
COLORS = list(filter(lambda x: BASE_COLORS.count(x) == 1, BASE_COLORS))


def one():
    return COLORS[random.randint(0, len(COLORS) - 1)]


def many(size):
    return [one() for i in range(size)]
