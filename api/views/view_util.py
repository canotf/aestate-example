from ajson import aj
from flask import request
from flask.views import MethodView


class MyView(MethodView):
    def get_data(self):
        return aj.load(request.get_data(as_text=True))

    def get_args(self):
        return request.args
