/*
 * @Author: LMN
 * @Title: API代理请求配置
 * @Date: 2021-02-20 11:19:43
 * @LastEditors: LMN
 * @LastEditTime: 2021-03-14 09:26:36
 * @Description: vue开发环境接口请求代理
 */
module.exports = {
  proxy: {
    // key值为接口前缀
    '/api': {
      // * 接口域名，这里需要配置
      target: 'http://127.0.0.1:8000',
      // 如果是https接口，需要配置这个参数
      secure: false,
      // 是否跨域
      changeOrigin: true,
      // 需要重写的
      pathRewrite: {
        /*
         * key为前端用的统一接口前缀，默认/api，与上面key值保持一致
         * value为后端用的统一接口前缀，一般为/api，如没有统一前缀改为''即可
         */
        // "^/api": "/api" // 这种接口配置出来     http://XX.XX.XX.XX:xxxx/api/login
        '^/api': '' // 这种接口配置出来     http://XX.XX.XX.XX:xxxx/login
      }
    }
  }
}
