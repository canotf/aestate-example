/*
 * @Author: LMN
 * @Title: 接口配置主文件
 * @LastEditors: Please set LastEditors
 * @Description: file content
 * @Date: 2021-03-14 11:10:45
 * @LastEditTime: 2021-06-03 16:15:53
 */
import axios from 'axios'

/**
 * 接口集合
 * key：接口名称
 * value：[静态json路径，动态api路径]
 */
const apis = {
  menu: ['/data/menu.json'],
  backstageMenu: ['/data/backstage-manage-menu.json'],
  login: ['/data/login.json', 'api/login'],
  register: ['/data/register.json', 'api/register'],
  sectionList: ['data/api_data/section-list.json', 'api/section_list'],
  noteList: ['data/api_data/note-list.json', 'api/note_list'],
  note: ['data/api_data/note.json', 'api/note'],
  comment: ['data/api_data/comment-list.json', 'api/comment_list'],
  addComment: ['data/api_data/addComment.json', 'api/add_comment'],
  deleteNote: ['data/api_data/deleteNote.json', 'api/delete_note'],
  addNote: ['data/api_data/addNote.json', 'api/add_note'],
  userList: ['data/api_data/user-list.json', 'api/user_list'],
  updateUserList: ['data/api_data/update-user-list.json', 'api/update_user_List'],
  deleteUser: ['data/api_data/delete-user.json', 'api/delete_user'],
  deleteSectionList: ['data/api_data/delete-section-list.json', 'api/delete_section'],
  editSection: ['data/api_data/edit-section.json', 'api/edit_section'],
  addSection: ['data/api_data/add-section.json', 'api/add_section'],
  editUserInfo: ['data/api_data/edit-user-info.json', 'api/edit_account_info'],
  userNoteList: ['data/api_data/user-note-list.json', 'api/user_note_list'],
  userChartLine: ['data/api_data/user-chart-line.json', 'api/userChartLine'],
  userChartBar: ['data/api_data/user-chart-bar.json', 'api/userChartBar'],
}

/**
 * 返回接口函数
 * 可在vue界面中通过this.$http(get||post, key, params)调用
 */
export default (type, key, param = {}, extra) => {
  // 用Promise提前处理接口是否请求成功
  const promise = new Promise((resolve, reject) => {
    // 判断请求方式是否有效
    if (type !== 'get' && type !== 'post') reject('请求方式必须为get或者post')
    // 判断是否包含有效接口
    if (!apis[key]) reject('接口名不存在')
    // 执行请求
    const url = apis[key][CONFIG.api] || apis[key][0]

    let params

    if (type === 'get') {
      params = {
        params: param
      }
    } else {
      params = param
    }

    if (url.slice(-5).toLowerCase() === '.json') {
      type = 'get'
      // params = null
    }

    axios[type](url, params)
      .then(res => {
        // 接口请求成功，预处理是否有数据
        if (res.data.status === 'success') {
          // 接口标识为成功，直接返回数据
          if (res.data[extra]) res.data.data.info = res.data[extra]
          resolve(res.data.data)
        } else {
          // 接口标识为失败，直接返回catch
          const info = '数据请求异常'
          reject(info)
        }
      })
      .catch(error => {
        // 接口请求失败，直接返回catch
        reject(error)
      })
  })
  return promise
}
