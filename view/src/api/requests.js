//引入 axios 模块
import axios from 'axios'
import { Message } from 'element-ui'
import Qs from 'qs'

//判断环境配置
const hosts = [
    'http://localhost:8000',
    // 'http://localhost:8001'
]
const baseUrl = process.env.NODE_ENV === 'development' ? hosts[0] : 'http://api.cacode.ren'  //   /api本地开发环境下的接口地址   '' 里面填写上线之后的接口地址


axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
// 创建一个axios实例

const intance = axios.create({
    baseURL: baseUrl,
    timeout: 50000,  //请求响应时间
})

// 请求拦截
intance.interceptors.request.use(config => {
    // 在请求之前要做的事
    config.data = Qs.stringify(config.data)
    return config
}, error => {
    //请求拦截失败之后
    // //('请求拦截失败');
    return Promise.reject(error.response)
})
// 响应拦截
intance.interceptors.response.use(response => {
    // 响应数据要做的事

    // //('响应拦截成功');
    // //(response);
    return response.data
}, error => {
    // 响应失败之后要做的事

    // //('响应拦截失败');
    Message.error({
        message: error.response.data.length < 100 ? error.response.data : "服务端错误",
        type: 'error',
        showClose: true,
        duration: 1000
    })
    return Promise.reject(error.response)
})
// get请求
export function getRequest(url, data = {}) {
    return intance.get(url, { params: data })
}

// post请求
export function postRequest(url, data = {}) {
    // data.token = store.state.token
    return intance.post(url, data)
}

// put请求
export function putRequest(url, data = {}) {
    return intance.put(url, data)
}

/** 
 * 
 * 图片上传  文件流方式
 * **/

// formdata 请求
const upLoadImage = axios.create({
    baseURL: baseUrl,
    // headers: {
    //     'Content-Type':'multipart/form-data'
    // }
})

// 响应拦截
upLoadImage.interceptors.response.use(response => {
    // 响应数据要做的事
    // //('响应拦截成功');
    return response.data
}, error => {
    // 响应失败之后要做的事
    // //('响应拦截失败');
    return Promise.reject(error)
})

export const upLoadRequest = (url, data = {}) => {
    return upLoadImage.post(url, data)
}

