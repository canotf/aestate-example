/*
 *     Created on 2018-06-28 22:46:25
 *     @author: LMN
 *     @summary: svtcharts-泳道图-模拟数据
 *     @version: v1.0.0
 *     Last Modified: 2018-06-28 22:49:01
 *     Modified By: limeina
 */
export default (start, end) => {
  // 时间区间
  const interval = [Number(start), Number(end)]
  // 返回数据
  const data = []
  // 最多显示4组数据
  const size = Math.floor(Math.random() * 4) + 1
  // 循环时间生成数据
  for (let i = 0; i < size; i++) {
    let d = []
    for (let j = interval[0]; j < interval[1]; j += 1000) {
      let ran = Math.random()
      if (ran < 0.000002) {
        let obj = {
          date: Number(j),
          // 数据值取100以内
          value: Math.floor(Math.random() * 100)
        }
        d.push(obj)
      }
    }
    data.push(d)
  }
  return data
}
