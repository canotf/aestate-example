/*
 *     Created on 2018-07-02 16:49:46
 *     @author: LMN
 *     @summary: svtcharts插件入口
 *     @version: v1.0.0
 *     Last Modified: '2018-07-02 16:52:15
 *     Modified By: limeina
 */
// 引入可用组件
import viewMain from './template/main'
import cMain from './template/common-main'
import svtTable from './template/svt-table.vue'
import svtPagination from './template/svt-pagination.vue'
import wrapperMain from './template/wrapper-header-footer.vue'
import checkboxTable from './template/checkbox-table.vue'

// 当前版本
const version = '0.0.1'
// 可用的组件集合，开发人员需将可用组件填入
const components = {
  viewMain,
  cMain,
  svtTable,
  svtPagination,
  wrapperMain,
  checkboxTable
}
// 最终svtcharts插件
const svtcharts = {
  version,
  // Vue插件开发必须有的公开方法，可通过vue.use注册插件
  install(Vue) {
    for (let key in components) {
      if (components.hasOwnProperty(key))
        Vue.component(components[key].name, components[key])
    }
  }
}
// 当vue为全局变量，直接注册插件
if (typeof window !== 'undefined' && window.Vue) window.Vue.use(svtcharts)
// 导出svtcharts插件
export default svtcharts
