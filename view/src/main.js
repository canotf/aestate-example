import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store/index'
import ElementUI from 'element-ui'
import VueQuillEditor from 'vue-quill-editor'
import axios from 'axios'
// import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/element-style.scss'
import 'echarts/extension/bmap/bmap'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 引入svtcharts
import svtcharts from '@/components/charts/svtcharts'
// 引入全局样式
import './assets/css/index.scss'
Vue.config.productionTip = false
import Message from 'element-ui';
// 全局应用element-ui
Vue.use(ElementUI)
// 全局应用quill编辑插件
Vue.use(VueQuillEditor)
// 全局应用svtcharts
Vue.use(svtcharts)
// 可直接全局调用一下类库，无需引入
// console.info(d3)
// console.info(L) leaflet Gis地图
// console.info(THREE)
// console.info($)
// console.info(CONFIG)
// console.info(echarts)

// Vue.prototype.$http("get", "sectionList")
//   .then(res => {
//     localStorage.setItem('sectionList', JSON.stringify(res.data))
//   })
//   .catch(err => {
//     console.info(err);
//   });
Vue.prototype.$axios = axios

axios.defaults.baseURL = 'http://127.0.0.1:8000/'

// // 添加一个返回拦截器

// axios.interceptors.response.use((response) => {
//   return response;
// }, (res) => {
//   Message.error(res.response.data.message);
//   return Re
// });

// axios.defaults.baseURL = 'http://127.0.0.1:8000/'
// 登录条件判断
// router.beforeEach(({ meta, path }, from, next) => {
//   const user = JSON.parse(localStorage.getItem('user'))

//   if (user) {

//     if (!user.time || Number(new Date()) - user.time > 24 * 60 * 60 * 1000) {
//       store.dispatch('loginOut')
//     }

//     if (!store.state.user) store.dispatch('login', user)

//     next()

//   } else {
//     next()
//   }
// })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
