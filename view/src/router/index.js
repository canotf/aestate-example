/* eslint-disable */
/*
 * @Author: LMN
 * @Title: 路由配置主文件
 * @LastEditors: Please set LastEditors
 * @Description: file content
 * @Date: 2021-03-14 11:18:24
 * @LastEditTime: 2021-06-06 15:18:30
 */
import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/home',
      name: "home",
      component: () => import('@/views/Home.vue'),
      children: [
        {
          path: '/home',
          name: 'all-notes',
          component: () => import('@/views/all-notes.vue'),
        },
        {
          path: '/home/:id',
          name: '根据指定板块id获取帖子',
          component: () => import('@/views/all-notes.vue'),
        },
        {
          path: '/note/:id',
          name: 'note',
          component: () => import('@/views/note.vue'),
        },
        {
          path: '/user',
          name: 'user-page',
          component: () => import('@/views/user-page.vue'),
        },
        {
          path: '/backstage-manage',
          redirect: '/user-manage',
          name: 'backstage-managete',
          component: () => import('@/views/backstage-manage.vue'),
          children: [
            {
              path: '/user-manage',
              name: 'user-manage',
              component: () => import('@/views/user-manage.vue'),
            },
            {
              path: '/section-manage',
              name: 'section-manage',
              component: () => import('@/views/section-manage.vue'),
            }, {
              path: '/visual',
              component: () => import('@/views/visual.vue'),
            },
            {
              path: '/tie',
              component: () => import('@/views/tie.vue'),
            },
            {
              path: '/lei',
              component: () => import('@/views/lei.vue'),
            },
          ]
        },
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue'),
    }
  ]
})
