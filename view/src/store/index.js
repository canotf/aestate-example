/*
 * @Author: LMN
 * @Title: file content
 * @LastEditors: Please set LastEditors
 * @Description: file content
 * @Date: 2021-03-14 13:34:59
 * @LastEditTime: 2021-06-06 15:39:58
 */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import api from '../api/index'

Vue.use(Vuex)
Vue.prototype.$ajax = axios
Vue.prototype.$http = api

export default new Vuex.Store({
  // 定义状态
  state: {
    user: null,
    userSelect: [
      [0, '超级管理员'], [1, '管理员'], [2, '用户']
    ]
  },
  // 更改状态的函数定义
  mutations: {
    // 保存登录
    login(state, item) {
      state.user = item
      localStorage.setItem('user', JSON.stringify(item))
    },
    // 清除登录
    loginOut(state) {
      state.user = null
      localStorage.setItem('user', null)
    },
  },
  // 异步操作分发
  actions: {
    // 登录
    login({ commit }, item) {
      commit('login', item)
    },
    // 登出
    loginOut({ commit }) {
      commit('loginOut')
    }
  }
})
