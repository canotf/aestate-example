/*
 * @Author: LMN
 * @Title: vue配置文件
 * @Date: 2021-02-18 15:33:09
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-06-04 14:26:26
 * @Description: vue项目相关配置项
 */
/*
 ***** 注意：以下配置信息不可修改 *****
 */
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const { proxy } = require('./proxy')
module.exports = {
  /*
   * process.env.NODE_ENV === 'production' 生产
   * process.env.NODE_ENV === 'development' 开发
   */
  // 部署应用包时的基本 URL
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',

  // 打包是否生成map文件
  productionSourceMap: !(
    process.env.NODE_ENV === 'production' && !process.env.ISTEST
  ),

  // 开发环境服务配置
  devServer: {
    // API请求代理
    proxy: proxy
  },

  configureWebpack: config => {
    config.plugins = config.plugins.concat([
      // 将一下包抛出全局变量
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        d3: 'd3',
        THREE: 'three',
        TWEEN: '@tweenjs/tween.js',
        L: 'leaflet',
        echarts: 'echarts'
      })
    ])
    if (process.env.NODE_ENV === 'production' && !process.env.ISTEST) {
      // 为生产环境修改配置...
      config.plugins = config.plugins.concat([
        new UglifyJsPlugin({
          uglifyOptions: {
            compress: {
              // warnings: false,
              drop_debugger: true,
              drop_console: true
            }
          },
          parallel: true
        })
      ])
    } else {
      // 为开发环境修改配置...
    }
  },

  lintOnSave: false
}
